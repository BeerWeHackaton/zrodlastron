﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HtmlAgilityPack;

namespace PoszukiwanieSource
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static String[] sourcesDictionary = { "source", "origin", "źródło", "źródła", "tekst oryginalny", "oryginał", "Ursprung",
                                            "origine", "πηγή", "de origen", "fuente", "šaltinis"};

        static public int licznikzrodel;
        static public int ogolnylicznikzrodel;

        public static String[] hrefValue = { "\0" };
        public static String[] adresy = { "\0" };
        public static string completeadres = "";

        static void SourceFound(String adres)
        {
            try
            {
                HtmlWeb web = new HtmlWeb();
                HtmlDocument document = web.Load(adres);
                hrefValue[0] = "\0";
                licznikzrodel = 0;
                for (var i = 0; i < sourcesDictionary.Length; i++)
                {
                    
                    foreach (HtmlNode link in document.DocumentNode.SelectNodes(".//a[@href]"))
                    {
                        if (link.ParentNode.InnerHtml.Contains(sourcesDictionary[i]))
                        {
                            if (document.DocumentNode.InnerHtml.ToString().Contains(sourcesDictionary[i]))
                            {
                                hrefValue[0] = link.GetAttributeValue("href", string.Empty);
                                licznikzrodel++;
                                break;
                            }
                        }
                    }
                    
                }
            }
            catch
            {
                MessageBox.Show("Wystąpił niespodziewany błąd!");
            }
        }

        private bool FindingDuplicates()
        {
            var ilosc = ListSources.Items.Count;
            if (ilosc == 1)
            {
                return false;
            }
            else
            {
                for (var i = 0; i < ilosc; i++)
                { var lancuch = completeadres;
                    var lancuch2 = ListSources.Items.GetItemAt(i).ToString();
                        if (lancuch == lancuch2)
                    { return true; }
                }
                    for (var i = 0; i < ilosc; i++)
                    {
                        var lancuch = ListSources.Items.GetItemAt(i).ToString();
                        for (var j = i + 1; j < ilosc; j++)
                        {
                            var lancuch2 = ListSources.Items.GetItemAt(j).ToString();
                            if (lancuch == lancuch2)
                            { return true; }
                        }
                    }
                } 
                return false;
            }
        

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var adresHTML = sourceAdress.Text;
            var poziom_zrodla = 1;
            

            if (!adresHTML.Contains("http://"))
            { completeadres = "http://" + adresHTML; }
            else
            { completeadres = adresHTML; }

            hrefValue[0] = completeadres;
            SourceTreeLabel.Visibility = System.Windows.Visibility.Visible;
            ListLabel.Visibility = System.Windows.Visibility.Visible;
            ListSources.Visibility = System.Windows.Visibility.Visible;

            do
            {
                    SourceFound(hrefValue[0]);
                    if (hrefValue[0] != "/0")
                    {
                        
                            textBlock.Text += "\n";
                            for (var i = 0; i < poziom_zrodla; i++)
                            {
                                textBlock.Text += "--";
                            }
                            textBlock.Text += hrefValue[0];
                            ListSources.Items.Add(hrefValue[0]);
                            ogolnylicznikzrodel++;
                       
                    }
                //for (var z = 0; z < adresy.Length; z++)
                //{
                //    adresy[z] = "\0";
                //    hrefValue[z] = "\0";
                //}
                poziom_zrodla++;
                if (FindingDuplicates())
                    break;
            }
            while (licznikzrodel != 0);
            sourceButton.IsEnabled = false;
            Reset.IsEnabled = true;
            if (FindingDuplicates())
                IsLoopLabel.Content = "Wystąpiła pętla źródeł";
            else IsLoopLabel.Content = "Brak pętli źródeł";

        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(ListSources.SelectedItem.ToString());
        }

        private void source_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sourceAdress.Text != "")
                sourceButton.IsEnabled = true;
            else sourceButton.IsEnabled = false;
        }

        private void ListSources_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            copyButton.IsEnabled = true;
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            sourceButton.IsEnabled = false;
            Reset.IsEnabled = false;
            sourceAdress.Text = "";
            SourceTreeLabel.Visibility = System.Windows.Visibility.Hidden;
            ListLabel.Visibility = System.Windows.Visibility.Hidden;
            ListSources.Visibility = System.Windows.Visibility.Hidden;
            IsLoopLabel.Content = "";
            textBlock.Text = "";
            ListSources.Items.Clear();
        }
    }
}
